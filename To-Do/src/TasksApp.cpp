#include "TasksApp.h"
#include "TasksDbHelper.h"

#include <bb/cascades/GroupDataModel>
#include <bb/cascades/ListView>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/QmlDocument>

using namespace bb::cascades;

TasksApp::TasksApp() {
}

TasksApp::~TasksApp() {
	delete mTasksDbHelper;
}

void TasksApp::onStart() {
	// Instantiate the database helper object.
	mTasksDbHelper = new TasksDbHelper();

	if (!loadQMLScene()) {
		qWarning("Failed to load QML scene.");
	}
}

bool TasksApp::loadQMLScene() {
	QmlDocument *qmlDocument = QmlDocument::create("asset:///main.qml");

	if (!qmlDocument->hasErrors()) {
		qmlDocument->setContextProperty("_taskApp", this);

		NavigationPane* navigationPane = qmlDocument->createRootObject<
				NavigationPane>();

		if (navigationPane) {
			QVariantList sqlData = mTasksDbHelper->loadDataBase("tasks.db",
					"tasks");

			if (!sqlData.isEmpty()) {
				mDataModel = navigationPane->findChild<GroupDataModel*>(
						"tasksModel");
				mDataModel->insertList(sqlData);

				mListView = navigationPane->findChild<ListView*>("tasksList");
			}

			Application::instance()->setScene(navigationPane);
			return true;
		}
	}

	return false;
}

void TasksApp::addNewRecord(const QString &prName, const QString &statusName,
		const QString &task) {
	QVariantMap map;
	map["pr"] = QString(prName);
	map["status"] = QString(statusName);
	map["task"] = QString(task);

	QVariant insertId = mTasksDbHelper->insert(map);

	if (!insertId.isNull()) {
		map["id"] = insertId;
		mDataModel->insert(map);
	}
}

void TasksApp::updateSelectedRecord(const QString &prName,
		const QString &statusName, const QString &task) {
	QVariantList indexPath = mListView->selected();

	if (!indexPath.isEmpty()) {
		QVariantMap itemMapAtIndex = mDataModel->data(indexPath).toMap();

		itemMapAtIndex["pr"] = QString(prName);
		itemMapAtIndex["status"] = QString(statusName);
		itemMapAtIndex["task"] = QString(task);

		mTasksDbHelper->update(itemMapAtIndex);
		mDataModel->updateItem(indexPath, itemMapAtIndex);
	}
}

void TasksApp::deleteRecord() {
	QVariantList indexPath = mListView->selected();

	if (!indexPath.isEmpty()) {
		QVariantMap map = mDataModel->data(indexPath).toMap();

		if (mTasksDbHelper->deleteById(map["id"])) {

			QVariantList categoryIndexPath;
			categoryIndexPath.append(indexPath.first());
			int childrenInCategory = mDataModel->childCount(categoryIndexPath);

			mDataModel->remove(map);

			if (childrenInCategory > 1) {
				int itemInCategory = indexPath.last().toInt();

				if (itemInCategory < childrenInCategory - 1) {
					mListView->select(indexPath);
				} else {
					indexPath.replace(1, QVariant(itemInCategory - 1));
					mListView->select(indexPath);
				}

			} else {
				QVariantList lastIndexPath = mDataModel->last();

				if (!lastIndexPath.isEmpty()) {
					if (indexPath.first().toInt()
							<= lastIndexPath.first().toInt()) {
						mListView->select(indexPath);
					} else {
						mListView->select(mDataModel->last());
					}
				}
			}
		} //end of inner if statement
	} // end of outer if statement
} // end of deleteRecord()

