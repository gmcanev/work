#include "tasksapp.h"

using ::bb::cascades::Application;

using namespace bb::cascades;

Q_DECL_EXPORT int main(int argc, char **argv)
{
	Application app(argc, argv);
	TasksApp mainApp;
	mainApp.onStart();
	return Application::exec();
}
