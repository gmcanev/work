import bb.cascades 1.0

Container {
    id: editControls
    property bool updateEnabled: false
    signal update()
    signal cancel()
    bottomPadding: 40

    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    Button {
        text: "Cancel"

        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }

        onClicked: {
            editControls.cancel();
        }
    }

    Label {
        id: editLabel
        text: "Edit Information"
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center

        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }

        textStyle.base: taskStyleLightBody.style
    }

    Button {
        id: updateButton
        text: "Update"
        enabled: updateEnabled

        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }

        onClicked: {
            editControls.update();
        }
    }
}// end of Container