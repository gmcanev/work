#ifndef _TASKSDBHELPER_H_
#define _TASKSDBHELPER_H_

#include <QtSql/QtSql>
#include <bb/data/SqlDataAccess>

using namespace bb::data;

class TasksDbHelper
{
public:
    TasksDbHelper();
    ~TasksDbHelper();

    QVariantList loadDataBase(const QString databaseName,
                              const QString table);
    bool deleteById(QVariant id);
    QVariant insert(QVariantMap map);
    bool update(QVariantMap map);

private:
    bool copyDbToDataFolder(const QString databaseName);
    bool queryDatabase(const QString query);

    QSqlDatabase mDb;
    QString mTable;
    QString mDbNameWithPath;
};

#endif
