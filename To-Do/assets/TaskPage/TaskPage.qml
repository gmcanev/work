import bb.cascades 1.0

Page {
    Container {
        background: backgroundPaint.imagePaint

        attachedObjects: [
            ImagePaintDefinition {
                id: backgroundPaint
                imageSource: "asset:///images/background.png"
            }
        ]

        layout: DockLayout {
        }

        TaskBubble {
            id: taskBubble
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
        }
    } // end of Container

    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: "Tasks"
            onTriggered: {
                nav.pop();
                taskBubble.editMode = false;
            }
        }
    }

    actions: [
        ActionItem {
            title: "Edit"
            imageSource: "asset:///images/Edit.png"
            ActionBar.placement: ActionBarPlacement.OnBar

            onTriggered: {
                taskBubble.editMode = true
            }
        },
        DeleteActionItem {
            objectName: "DeleteAction"
            title: "Delete"

            onTriggered: {
                _taskApp.deleteRecord();
                taskBubble.editMode = false
            }
        }
    ]

    attachedObjects: [
        TextStyleDefinition {
            id: taskStyleLightBody
            base: SystemDefaults.TextStyles.BodyText
            color: Color.create("#fafafa")
        }
    ]
}// end of Page
