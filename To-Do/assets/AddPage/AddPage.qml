import bb.cascades 1.0

Page {
    id: addPage
    signal addPageClose()

    titleBar: TitleBar {
        id: addBar
        title: "Add"
        visibility: ChromeVisibility.Visible

        dismissAction: ActionItem {
            title: "Cancel"
            onTriggered: {
                addPage.addPageClose();
            }
        }

        acceptAction: ActionItem {
            title: "Save"
            onTriggered: {
                _taskApp.addNewRecord(prNameField.text, statusNameField.text, taskField.text);
                addPage.addPageClose();
            }
        }
    }

    Container {
        id: editPane
        property real margins: 40
        background: Color.create("#f8f8f8")
        topPadding: editPane.margins
        leftPadding: editPane.margins
        rightPadding: editPane.margins

        layout: DockLayout {
        }

        Container {
            layout: StackLayout {
            }

            TextArea {
                id: taskField
                hintText: "Task description"
                topMargin: editPane.margins
                bottomMargin: topMargin
                enabled: false
                preferredHeight: 450
                maxHeight: 450
                horizontalAlignment: HorizontalAlignment.Fill
            }

            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                TextField {
                    id: prNameField
                    rightMargin: editPane.margins
                    hintText: "Priority"
                    enabled: false
                }

                TextField {
                    id: statusNameField
                    hintText: "Status"

                    onTextChanging: {
                        if (text.length > 0) {
                            addPage.titleBar.acceptAction.enabled = true;
                            taskField.enabled = true;
                            prNameField.enabled = true;
                        } else {
                            addPage.titleBar.acceptAction.enabled = false;
                            taskField.enabled = false;
                            prNameField.enabled = false;
                        }
                    }
                }
            }
        }
    }

    function newTask() {
        prNameField.text = "";
        statusNameField.text = "";
        taskField.text = "";
        addPage.titleBar.acceptAction.enabled = false;
        taskField.enabled = false;
        prNameField.enabled = false;
    }
}