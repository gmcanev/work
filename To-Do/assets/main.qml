import bb.cascades 1.0
import "AddPage"

NavigationPane {
    id: nav
    property variant _contentView
    property bool addShown: false
    Page {
        id: taskListPage
        Container {

            ListView {
                id: tasksList
                objectName: "tasksList"

                layout: StackListLayout {
                    headerMode: ListHeaderMode.Sticky
                }
                dataModel: tasksModel

                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        StandardListItem {
                            imageSpaceReserved: false
                            title: ListItemData.task
                        }
                    },
                    ListItemComponent {
                        type: "header"
                        Header {
                            title: ListItemData
                        }
                    }
                ]

                onTriggered: {

                    select(indexPath);

                    var page = taskPageDefinition.createObject();
                    nav.push(page);
                }

                onSelectionChanged: {
                    if (selected) {
                        var chosenItem = dataModel.data(indexPath);

                        if (chosenItem.pr == undefined) {
                            chosenItem.pr = "";
                        }

                        _contentView = chosenItem;
                    }
                }

                attachedObjects: [
                    GroupDataModel {
                        id: tasksModel
                        objectName: "tasksModel"
                        grouping: ItemGrouping.ByFullValue
                        sortingKeys: [ "status" ]

                        onItemAdded: {
                            if (addShown) {
                                if (nav.top == taskListPage) {
                                    tasksList.clearSelection();
                                    tasksList.select(indexPath);
                                    tasksList.scrollToItem(indexPath, ScrollAnimation.Default);

                                    var page = taskPageDefinition.createObject();
                                    nav.push(page);
                                }
                            }
                        }

                        onItemRemoved: {
                            var lastIndexPath = last();
                            if (lastIndexPath[0] == undefined) {
                                if (nav.top != taskListPage) {
                                    nav.popAndDelete();
                                }
                            }
                        }

                        onItemUpdated: {
                            var chosenItem = data(indexPath);
                            _contentView = chosenItem;
                        }
                    } // end of GroupDataModel
                ] // end of attachedObjects
            } // end of ListView
        } // end of Container

        attachedObjects: [
            Sheet {
                id: addSheet
                AddPage {
                    id: add
                    onAddPageClose: {
                        addSheet.close();
                    }
                }
                onClosed: {
                    add.newTask();
                }
            },
            ComponentDefinition {
                id: taskPageDefinition
                source: "TaskPage/TaskPage.qml"
            }
        ]        // end of attachedObjects

        actions: [
            ActionItem {
                title: "Add"
                imageSource: "asset:///images/Add.png"
                ActionBar.placement: ActionBarPlacement.OnBar
                onTriggered: {
                    addSheet.open();
                    nav.addShown = true;
                }
            }
        ] // end of attachedObjects
    } // end of Page

    onTopChanged: {
        if (page == taskListPage) {
            tasksList.clearSelection();
        }
    }

    onPopTransitionEnded: {
        page.destroy();
    }
}// end of NavigationPane