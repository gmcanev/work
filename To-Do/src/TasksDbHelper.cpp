#include "tasksdbhelper.h"

using namespace bb::data;

TasksDbHelper::TasksDbHelper() {
}

TasksDbHelper::~TasksDbHelper() {
	if (mDb.isOpen()) {
		QSqlDatabase::removeDatabase(mDbNameWithPath);
		mDb.removeDatabase("QSQLITE");
	}
}

bool TasksDbHelper::copyDbToDataFolder(const QString databaseName) {
	QString dataFolder = QDir::homePath();
	QString newFileName = dataFolder + "/" + databaseName;
	QFile newFile(newFileName);

	if (!newFile.exists()) {
		QString appFolder(QDir::homePath());
		appFolder.chop(4);
		QString originalFileName = appFolder + "app/native/assets/sql/"
				+ databaseName;
		QFile originalFile(originalFileName);

		if (originalFile.exists()) {
			return originalFile.copy(newFileName);
		} else {
			qDebug() << "Failed to copy file, database file does not exist.";
			return false;
		}
	}

	return true;
}
QVariantList TasksDbHelper::loadDataBase(const QString databaseName,
		const QString table) {
	QVariantList sqlData;

	if (copyDbToDataFolder(databaseName)) {
		mDbNameWithPath = "data/" + databaseName;

		SqlDataAccess sqlDataAccess(mDbNameWithPath);

		sqlData = sqlDataAccess.execute("select * from " + table).value<
				QVariantList>();

		if (sqlDataAccess.hasError()) {
			DataAccessError err = sqlDataAccess.error();
			qWarning() << "SQL error: type=" << err.errorType() << ": "
					<< err.errorMessage();
			return sqlData;
		}
		mDb = QSqlDatabase::addDatabase("QSQLITE",
				"database_helper_connection");
		mDb.setDatabaseName(mDbNameWithPath);

		if (!mDb.isValid()) {
			qWarning()
					<< "Could not set database name, probably due to an invalid driver.";
			return sqlData;
		}

		bool success = mDb.open();

		if (!success) {
			qWarning() << "Could not open database.";
			return sqlData;
		}

		mTable = table;
	}

	return sqlData;
}
bool TasksDbHelper::deleteById(QVariant id) {
	if (id.canConvert(QVariant::String)) {
		QString query = "DELETE FROM " + mTable + " WHERE id=" + id.toString();
		return queryDatabase(query);
	}

	qWarning() << "Failed to delete item with id: " << id;

	return false;
}
QVariant TasksDbHelper::insert(QVariantMap map) {

	QSqlQuery sqlQuery(mDb);

	sqlQuery.prepare(
			"INSERT INTO " + mTable
					+ " (pr, status, task)" "VALUES(:prName, :statusName, :task)");

	sqlQuery.bindValue(":prName", map["pr"]);
	sqlQuery.bindValue(":statusName", map["status"]);
	sqlQuery.bindValue(":task", map["task"]);
	sqlQuery.exec();

	QSqlError err = sqlQuery.lastError();

	if (err.isValid()) {
		qWarning() << "SQL reported an error : " << err.text();
	}

	return sqlQuery.lastInsertId();
}
bool TasksDbHelper::update(QVariantMap map) {
	QSqlQuery sqlQuery(mDb);

	sqlQuery.prepare(
			"UPDATE " + mTable
					+ " SET pr=:prName, status=:statusName, task=:task WHERE id=:id");

	sqlQuery.bindValue(":prName", map["pr"]);
	sqlQuery.bindValue(":statusName", map["status"]);
	sqlQuery.bindValue(":task", map["task"]);
	sqlQuery.bindValue(":id", map["id"].toString());
	sqlQuery.exec();

	QSqlError err = sqlQuery.lastError();

	if (!err.isValid()) {
		return true;
	}

	qWarning() << "SQL reported an error : " << err.text();

	return false;
}
bool TasksDbHelper::queryDatabase(const QString query) {

	QSqlQuery sqlQuery(query, mDb);

	QSqlError err = sqlQuery.lastError();

	if (err.isValid()) {
		qWarning() << "SQL reported an error for query: " << query << " error: "
				<< mDb.lastError().text();
		return false;
	}

	return true;
}
