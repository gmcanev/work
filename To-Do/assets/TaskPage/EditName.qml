import bb.cascades 1.0

Container {
    id: editName
    property alias prName: prNameField.text
    property alias statusName: statusNameField.text
    signal enableSave(bool enable)

    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }

    TextField {
        id: prNameField
        hintText: "Priority"

        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }

        text: _contentView.pr
    }

    TextField {
        id: statusNameField
        hintText: "Status"
        text: _contentView.status

        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }

        onTextChanging: {
            if (text.length > 0) {
                editName.enableSave(true);
                prNameField.enabled = true;
            } else {
                editName.enableSave(false);
                prNameField.enabled = false;
            }
        }
    }
}