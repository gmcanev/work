import bb.cascades 1.0

Container {
    id: taskBubble
    property bool editMode: false

    topPadding: 30
    bottomPadding: topPadding
    rightPadding: topPadding
    leftPadding: topPadding

    EditControls {
        id: editControls
        visible: taskBubble.editMode

        onCancel: {
            longText.text = _contentView.task;
            _taskApp.updateSelectedRecord(_contentView.pr, _contentView.status, _contentView.task);
            taskBubble.editMode = false;
        }

        onUpdate: {
            _taskApp.updateSelectedRecord(editName.prName, editName.statusName, longText.text);
            taskBubble.editMode = false;
        }
    } // end of EditControls

    Container {
        horizontalAlignment: HorizontalAlignment.Center

        layout: DockLayout {
        }

        ImageView {
            imageSource: "asset:///images/border_bubble.amd"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }

        Container {
            topPadding: 54
            bottomPadding: 85
            rightPadding: 30
            leftPadding: rightPadding

            TextArea {
                id: longText
                preferredWidth: 520
                editable: taskBubble.editMode

                text: _contentView.task
            }
        } // end of text area Container
    } // end of task Container

    Container {
        topPadding: 15

        layout: DockLayout {
        }

        Label {
            id: nameLabel
            visible: ! taskBubble.editMode

            text: "Priority: " + _contentView.pr + ", Status: " + _contentView.status
            textStyle.base: taskStyleLightBody.style
        }

        EditName {
            id: editName
            visible: taskBubble.editMode
            onEnableSave: {
                if (enable) {
                    editControls.updateEnabled = true;
                    longText.enabled = true;
                } else {
                    editControls.updateEnabled = false;
                    longText.enabled = false;
                }
            }
        }
    } // end of task author Container
}// end of actual task Container
