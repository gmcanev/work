#ifndef TASKSAPP_H
#define TASKSAPP_H

#include <bb/cascades/Application>
#include <bb/cascades/DataModel>
#include <bb/data/SqlDataAccess>
#include <QObject>

using namespace bb::cascades;
using namespace bb::data;

namespace bb
{
    namespace cascades
    {
        class GroupDataModel;
        class ListView;
        class NavigationPane;
    }
}

class TasksDbHelper;

class TasksApp: public QObject
{
Q_OBJECT

public:
    TasksApp();
    ~TasksApp();

    void onStart();

    Q_INVOKABLE
    void addNewRecord(const QString &prName,
                      const QString &statusName,
                      const QString &task);

    Q_INVOKABLE
    void updateSelectedRecord(const QString &prName,
                              const QString &statusName,
                              const QString &task);

    Q_INVOKABLE
    void deleteRecord();

private:
    bool loadQMLScene();

    TasksDbHelper *mTasksDbHelper;
    GroupDataModel *mDataModel;
    ListView *mListView;
};

#endif
